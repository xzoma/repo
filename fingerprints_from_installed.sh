#!/bin/sh
# This can be used to conveniently get the certificate fingerprints of installed APKs from an Android device over adb.
# Requirements: adb and either apksigner or androguard
# Usage: ./fingerprints_from_installed.sh <app-id1> <app-id2> ...
for i in "$@"; do
    path=$(adb shell pm path "$i" | grep base.apk | cut -d: -f2)
    tmp_dir="/tmp/tempapks"
    mkdir -p $tmp_dir
    adb pull "$path" "$tmp_dir/$i.apk"
    apksigner verify --print-certs "$tmp_dir/$i.apk" | grep -i sha-256
    #androguard sign --hash sha256 "$tmp_dir/$i.apk" | grep -i sha256
done
rm -rf $tmp_dir
